#include <stdio.h>
#include <stdlib.h>

char token;

int exp(void);
int term(void);
int factor(void);

char resultS[100];
int i = 0;

void GravarNumero(char c){
	resultS[i] = c;
	i++;
}

void error(void){
	fprintf(stderr, "Error\n");
	exit(1);
}

void match(char expectedToken){
	if (token = expectedToken) token = getchar();
	else error();
}

int main(int argc, char const *argv[]){
	int result;
	token = getchar();
	GravarNumero(token);

	result = exp();
	if (token == '\n') {
		printf("Result = %d\n", result );
		printf("Result pos-fixo: %s\n",resultS );
	}
	else error();
	return 0;
}

int exp(void){
	int temp = term();
	int aux = temp;
	char aux2;
	while ((token == '+') || (token == '-')){
		switch(token){
			case '+':
				aux2 = token;
				match('+');
				aux = term();
				temp+=aux;

				GravarNumero((aux+48));
				GravarNumero(aux2);
				break;
			case '-':
				aux2 = token;
				match('-');
				aux = term();
				temp-=aux;

				GravarNumero((aux+48));
				GravarNumero(aux2);
				break;
		}
	}
	return temp;
}


int term(void){
	int temp = factor();
	int aux = temp;
	char aux2;
	while (token == '*'){
		aux2 = token;
		GravarNumero((temp+48));
		GravarNumero(token);
		match('*');
		aux = factor();
		temp*= aux;

		GravarNumero((aux+48));
		GravarNumero(aux2);
	}
	return temp;
}

int factor(void){
	int temp;
	if (token == '('){
		GravarNumero(token);
		match('(');
		temp = exp();
		match(')');
		GravarNumero(token);
	}
	else if( isdigit(token)){
		ungetc(token,stdin);
		scanf("%d", &temp);
		token = getchar();
	}
	else error();
	return temp;
}