%{
	/*	lex 2.6.0
		Linux segundo-Inspiron 4.4.0-116-generic #140-Ubuntu SMP Mon Feb 12 21:23:04 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux
	 */
	
	#include <stdio.h>
	#ifndef FALSE
	#define FALSE 0
	#endif
	#ifndef TRUE
	#define TRUE 1
	#endif
	#ifndef yywrap
	static int yywrap (void) { return 1; }
	#endif
%}

%% 

"/*" { 	char c;
		int done = FALSE;
		ECHO;
		do{	
			while((c=input())!='*')
				putchar(toupper(c));
			putchar(toupper(c));
			while((c=input())=='*')
				putchar(toupper(c));
			putchar(toupper(c));

			if(c == '/') done = TRUE;

		}while(!done);
	
}

%%

void main(void){
	yyin = fopen("../teste.c", "r" ); 
	yylex();
}
