%{
	/*	lex 2.6.0
		Linux segundo-Inspiron 4.4.0-116-generic #140-Ubuntu SMP Mon Feb 12 21:23:04 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux
	 */
	
	#include <stdio.h>
	#include <string.h>	
	int conta_linha = 1;
	int conta_palavra = 0;
	int conta_caractere = 0;
	#ifndef yywrap
	static int yywrap (void) { return 1; }
	#endif
%}

linha \n

digit [0-9]
numero {digit}+
caractere (([a-z])|([A-Z]))
word ({caractere}+|{caractere}+{numero}|{numero})

%% 

{linha} {conta_linha++;}
{word} {conta_palavra++; conta_caractere += strlen(yytext);}


%%

void main(void){
	/**/
	
	yyin = fopen("../teste.c", "r" ); 
	yylex();

	FILE * pFile;
	pFile = fopen ("saida.txt","w");

	fprintf(pFile, "palavras:  %d\ncaracteres: %d\nlinha: %d\n", conta_palavra,conta_caractere, conta_linha);
}
