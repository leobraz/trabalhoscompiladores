%{
	/*	lex 2.6.0
		Linux segundo-Inspiron 4.4.0-116-generic #140-Ubuntu SMP Mon Feb 12 21:23:04 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux
	 */


	#include <stdio.h>
	#ifndef FALSE
	#define FALSE 0
	#endif
	#ifndef TRUE
	#define TRUE 1
	#endif
	int inComment = FALSE;

	#ifndef yywrap
	static int yywrap (void) { return 1; }
	#endif
%}


%%

[A-Z] { 
		if(!inComment){
			putchar(tolower(yytext[0]));
		}else{
			putchar(yytext[0]);
		}

	}

"/*" {
	ECHO;
	inComment = TRUE;
	}

"*/" {
	ECHO;
	inComment = FALSE;
	} 


%%

void main(void){
	yyin = fopen("../teste.c", "r" ); 
	yylex();
}

