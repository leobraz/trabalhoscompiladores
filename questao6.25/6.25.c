#include <stdlib.h>
#define base_num 1
#define basecar 2
#define num 3
#define digito 4
#define erro -1



struct arvoreno{
	struct arvoreno* dir;
	struct arvoreno* esq;

	float val;
	char filho;
	int base;
	int no_tipo;

};

typedef struct arvoreno arvoreno;


void AvalComBase(arvoreno* T){

	switch (T->no_tipo){

		case base_num:
			AvalComBase(T->dir);
			T->esq->base = T->dir->base;
			AvalComBase(T->esq);
			T->val = T->esq->val;
			break;

		case num:
			T->esq->base = T->base;
			AvalComBase(T->esq);
			if (T->dir != NULL){
				T->dir->base = T->base;
				AvalComBase(T->dir);
				if (T->esq->val != erro && T->dir->val != erro){
					T->val = (T->base * T->esq->val) + T->dir->val;
				}else{
					T->val = erro;
				}
			}
			break;

		case basecar:
			if (T->filho == 'o'){
				T->base = 8;
			}else{
				T->base = 10;
			}
			break;

		case digito:
			if (T->base == 8 && (T->esq->val == 8 || T->esq->val == 9 || T->dir->val == 8 || T->dir->val == 9)){
				T->val = erro;
			}else{
				T->val = T->esq->val;
			}

	}

}

int main(){
	arvoreno* T;
	AvalComBase(T);
	return 0;
}